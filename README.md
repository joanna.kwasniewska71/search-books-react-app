# My App - Books searcher

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Installation

1. Clone the repository: git clone https://gitlab.com/joanna.kwasniewska71/search-books-react-app

2. Navigate to the project folder: cd search-books-react-app

3. Install dependencies: `npm install`

## Getting Started

After installing the dependencies, you can run the application locally

You put in command line =>  `npm start`

Open http://localhost:3000 with your browser to see the result.

Have fun :)

## Test application

You put in command line =>  `npx react-scripts test`

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


