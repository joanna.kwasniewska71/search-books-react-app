import React from "react";
import { getCloseNumbers } from "src/utils/getCloseNumbers";
import "./Pagination.scss";


interface PaginationProps {
  currentPage: number;
  totalPages: number;
  onPageChange: (page: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({ currentPage, totalPages, onPageChange }) => {
  const pagesToRender = getCloseNumbers(currentPage, totalPages, 2);

  return (
    <div className="pagination">
      {pagesToRender.map((page) => (
        <span key={page}>
          <button
            className={`pagination-button ${currentPage === page ? "active" : ""}`}
            onClick={() => currentPage !== page && onPageChange(page)}
          >
            {page}
          </button>
        </span>
      ))}
    </div>
  );
};

export default Pagination;
