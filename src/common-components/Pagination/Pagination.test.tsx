import { render, fireEvent } from "@testing-library/react";
import Pagination from "./Pagination";

describe("Pagination Component", () => {
  
  it("renders pagination with the current page correctly", () => {
    const { container } = render(
      <Pagination currentPage={3} totalPages={10} onPageChange={() => {}} />
    );

    expect(container).toMatchSnapshot();
  });

  it("calls onPageChange when a different page is clicked", () => {
    const onPageChangeMock = jest.fn();

    const { getByText } = render(
      <Pagination currentPage={3} totalPages={10} onPageChange={onPageChangeMock} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.click(getByText("5"));

    expect(onPageChangeMock).toHaveBeenCalledWith(5);
  });

  it("does not call onPageChange when the current page is clicked", () => {
    const onPageChangeMock = jest.fn();

    const { getByText } = render(
      <Pagination currentPage={3} totalPages={10} onPageChange={onPageChangeMock} />
    );

    // eslint-disable-next-line testing-library/prefer-screen-queries
    fireEvent.click(getByText("3"));

    expect(onPageChangeMock).not.toHaveBeenCalled();
  });
});
