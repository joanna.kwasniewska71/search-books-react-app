import "./App.scss";
import Result from "../Result/Result";

function App() {
  return (
    <div className="App">
      <h1 className="title-app">Books Search App</h1>
      <Result />
    </div>
  );
}

export default App;
