import React, { useState } from "react";
import List from "../BooksList/BooksList";
import SearchBar from "../../common-components/SearchBar/SearchBar";
import { useBookSearch } from "../../hooks/useSeach";
import Pagination from "../../common-components/Pagination/Pagination";

const Result: React.FC = () => {
  const { query, books, loading, searchBooks, totalItems, ITEMS_PER_PAGE } = useBookSearch();
  const [currentPage, setCurrentPage] = useState(1);
  const totalPages = Math.ceil(totalItems / ITEMS_PER_PAGE);

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
    searchBooks(query, page - 1);
  };

  if (loading) return <h2>Loading...</h2>;

  return (
    <div className="App">
      <SearchBar
        onSearch={(query) => {
          searchBooks(query);
          setCurrentPage(1);
        }}
      />
      {books !== undefined ? <List books={books} /> : <h2>No items</h2>}
      {totalItems > ITEMS_PER_PAGE && (
        <Pagination
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={handlePageChange}
        />
      )}
    </div>
  );
};
export default Result;
