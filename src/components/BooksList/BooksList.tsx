import React from "react";
import "./BooksList.scss";

const PLACEHOLDER_URL =
  "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/No-Image-Placeholder.svg/330px-No-Image-Placeholder.svg.png?20200912122019";

export interface Book {
  volumeInfo: {
    title: string;
    authors?: string[];
    imageLinks: { smallThumbnail?: string };
  };
}

interface BookListProps {
  books: Book[] | null;
}

const BooksList: React.FC<BookListProps> = ({ books }) => {
  return (
    <div className="books-list-wrapper">
      {books?.map(({ volumeInfo }, index) => {
        const { title, imageLinks, authors } = volumeInfo;
        return (
          <div key={index} className="book-wrapper">
            {imageLinks ? (
              <img className="image-book" src={imageLinks?.smallThumbnail} alt={title} />
            ) : (
              <img className="image-book" src={PLACEHOLDER_URL} alt={title} />
            )}
            <div>
              <h3>{title}</h3>
              {authors && (
                <p>
                  Authors:&nbsp;
                  {authors?.map((author, index) => {
                    const lastItem = index === authors.length - 1;
                    return (
                      <span key={index}>
                        {author}
                        {!lastItem && ", "}
                      </span>
                    );
                  })}
                </p>
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default BooksList;
