import { useCallback, useState } from "react";
import { BASE_URL } from "../api/config";
import { Book } from "../components/BooksList/BooksList";

interface BookSearchResult {
  items: Book[];
  totalItems: number;
}

export function useBookSearch() {
  const [query, setQuery] = useState<string>("");
  const [books, setBooks] = useState<Book[] | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [totalItems, setTotalItems] = useState<number>(0);
  const ITEMS_PER_PAGE = 10;

  const searchBooks = useCallback((searchQuery: string, page: number = 0) => {
    const startIndex = page * ITEMS_PER_PAGE;

    setQuery(searchQuery);
    setLoading(true);

    fetch(`${BASE_URL}?q=${searchQuery}&startIndex=${startIndex}`)
      .then((response) => response.json())
      .then((data: BookSearchResult) => {
        setBooks(data.items);
        setTotalItems(data.totalItems);
        setLoading(false);
      })
      .catch(() => {
        console.error("Error occurred while fetching data.");
        setLoading(false);
      });
  }, []);

  return { query, books, loading, searchBooks, totalItems, ITEMS_PER_PAGE };
}
