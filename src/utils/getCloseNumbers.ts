export function getCloseNumbers(pointNumber: number, total: number, range: number) {
  const closeNumbers: number[] = [];

  for (let i = Math.max(1, pointNumber - range); i <= Math.min(pointNumber + range, total); i++) {
    if (i >= 1 && i <= total) {
      closeNumbers.push(i);
    }
  }

  return closeNumbers;
}
